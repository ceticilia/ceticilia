# Referenda and elections act 2023

<center> This act aims to clear up the confusing process of passing a bill via referenda </center>

## MERI

- This act gives the MERI the additional responsibility of sending out a weekly helios ballot for referenda every monday, this ballot should be open for 48 hours.
- This act gives the MERI the additional responsibility of maintaining a document detailing secure ways to create an emergency referendum.
- To the end of preventing election fraud **All** new citizens must be vetted by MERI.
- The MERI shall consist of one member of each party that has atleast 4 members and has existed for at least a month.
- For the purposes of this legistlation a party is defined as a political organization created for the purpose of participating in Ceticilian elections and representing a set of political ideals.

## Procedure for notifying the MERI that you intend to initiate a referenda

In order to get a referendum onto the weekly helios ballot one should follow the following steps:

- Start a petition (this step is not required for emergency referenda).
- If and when the petition gets the number of signatures prescribed by the constitution you should send a message to the head of MERI with a copy of the petition.
- In this message you should include the text of the referenda and the accompanying bill.
- If the referendum is an emergency you should inform the MERI of this in your notification, they will then consider if your referendum truely is an emergency. If the MERI decides your referendum is not an emergency then you have the right to appeal your case before a judge who is then empowered to force the MERI to treat your ballot as an emergency.
- If a case is sucessfully made to have a ballot treated as an emergency then the MERI is required to immediatly send out a helios ballot with your referenda in, this ballot should last no shorter then one hour but no longer then 24 hours. If you deem the timeframe set by the MERI as too short or too long you are allowed to appeal to a sitting judge who is empowered to force the MERI to set a different timeframe.
- In the event that the MERI refuses or is unable to send a ballot then a citizen is empowered to make a referendum through whatever secure means they have at their disposal. They are to do this adhering to the guidelines set down by MERI. In the event that the MERI doesn't lay down guidelines or their guidelines are vexatious then the person seeking a referenda can get a sitting judge to approve an alternate method.

## Referenda and electoral offences
### Impeding someones right to vote

To commit the offence of impeding someones right to vote one must somehow interfere with a person casting their ballot, this can take many forms including but not limited to:

- Someone (A) causing the person (B) to not recive their ballot
- Someone (A) by means of fraud, hacking or coersion make it apear that someone (B) has voted a certain way or force them to vote a certain way
- Someone (A) tampering with ballots so that people (B) think they are voting for something different then they are

A person convicted of the offence of impeding someones right to vote shall be barred from serving at the MERI for up to an indefinate time and barred from holding office for a period not exceeding 6 months.

### Voter fraud

to commit voter fraud one must cause a petition or referendum to have more votes then it would otherwise have, this can take many forms including but not limmited too

- Seting up puppet accounts or citizens to vote on referenda and sign petitions
- Tampering with electoral systems to report false results
- Declaring results that are incorect as a representitive of MERI

A person convicted of voter fraud shall be bared from serving at the MERI for up to an indefinate time and/or barred from holding office for up to 6 months.
