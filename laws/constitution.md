_Constitution of Ceticilia as of 4 January 2022_

Ceticilia, aware of its rights and duties internationally and deriving its own validity out of its democratic foundation and principles towards global peace and the welfare of all peoples, finds the following constitutional text immediately binding.

# Article 1 -- Basis

1.  The name of this nation is Ceticilia.
2.  The flag of this nation is a flag with following attributes:
    1.  The top and bottom quarters, horizontally, are colored gold.
    2.  The central two quarters, horizontally, are colored white.
    3.  The center of the flag features a dove, facing right, with an olive branch in its beak.
    4.  The Government Seal of this nation is a heraldic achievement featuring following attributes:
        1.  A Square Iberian, divided once by Fess and the resulting top being divided by Pale. The Square Iberian and all its divisions feature a Sable fimbriation.
        2.  The Dexter Chief division features a dove, facing right, with an olive branch in its beak.
        3.  The Sinister Chief division is divided into three equal horizontal bands alternating between Or and Argent tincture. The divisions are to be fimbriated Sable.
        4.  The Middle Base division features a chain link of three, breaking in the middle.
        5.  A Sable libra as a crown.
        6.  Ionic Greek columns alternating between Or and Argent tincture as the bilateral supporters.
        7.  A Motto on the base, reading the name of the nation.
    5.  The Government Service Seal of this nation is a heraldic achievement with following attributes:
        1.  An Argent French with Or fimbriations, featuring a dove facing right carrying an olive branch in its beak.
        2.  Or Laurel Branches surrounding the Argent French.
        3.  The Government Service Seal is used on documents on the person and by citizens\' offices. In all other circumstances, the Government Seal is used.
    6.  The national languages are Cet, English, German, and Spanish. The minority languages are Toki Pona, Ripuarian, and Japanese.
    7.  Cet is co-official nationwide and must be taught in all school curricula.

# Article 2 - Human rights

1.  Human dignity must be respected and protected.
2.  All humans are equal before the law.
3.  Religious freedom is guaranteed.
4.  Expressive freedom is guaranteed.
5.  Freedom of assembly is guaranteed.
6.  The right to life and physical integrity is guaranteed.
7.  The right to secrecy of telecommunication and of the home is guaranteed.
8.  The right to information from independent sources is guaranteed.
9.  Reproductive rights are guaranteed.
10. All human rights find their limits where they infringe on the basic human rights of others.
11. Discrimination based on creed, disability, ethnicity, gender identity, origin, religion, sex or sexuality in employment or before the law is prohibited.

# Article 3 -- Ceticilian Principles

1.  All power emanates from the people.
2.  No legal consequences may be retroactively altered due to an alteration in law.
3.  No military conflict may be initiated by Ceticilia. The President will instruct the military if Ceticilia must respond defensively to a military threat.
4.  No Ceticilian may be compelled unto arms against their conscience.
5.  Constitutional law takes precedence over all other law as constitutional law is the basis of Ceticilian corpus of law. Laws that contradict the Constitution are void.
6.  Recent law takes precedence over past law.
7.  Ceticilia is anti-fascist and outlaws all symbolism that exalt fascism outside of educational settings or in critical art. No fascist is to become a Ceticilian citizen, nor is fascism a valid ideology for any organization or government in Ceticilia.

# Article 4 -- Citizenship

1.  A citizen is defined as any one alive person or alive system with one physical human body that was granted Ceticilian citizenship.
2.  Citizenship is terminated through the following means:
    1.  Voluntary termination of citizenship, through either leaving the forums or by stating it to the appropriate authority.
    2.  Ostracism by a two-thirds vote by the legislature.
    3.  Ostracism by the judiciary for causing danger to the safety of Ceticilian public or advertising fascism.
3.  Every citizen may vote in all public referenda and elections.
4.  Every citizen may apply for any elected office.

# Article 5 -- Elections

1.  An election period is defined as the period of time from the beginning of Friday to the end of Saturday, lasting for a total of forty-eight hours.
2.  Each election must be conducted on ans election period.
3.  Electoral issues that fall into the same timeframe are conducted on the same ballot.
4.  No voter be obstructed from casting their vote. Ballots must be honest, fair, and simple to understand for people of all abilities.
5.  Coordinated Universal Time (UTC) is used for elections.
6.  In the event of a winning tie between two or more candidates in a Presidential election, the CetÞing elects the President from among the tied candidates with a simple majority or plurality vote lasting 24 hours. If no candidate has received more votes than every other candidate by the end of the voting period, the Supreme Court must elect a candidate from among those who tied for most votes in the CetÞing.

# Article 6 -- Legislative

1.  The legislature of Ceticilia consists of a parliament, referred to as the CetÞing, and the people via public referenda. The CetÞing\'s members are referred to as Members of Parliament or Members of the CetÞing.
2.  Laws presented by the CetÞing and by the people via referenda may be enacted by either without the approval of the other. Laws become effective on the next election period that is more than seven days ahead, unless otherwise specified.
3.  The following regulations apply to the CetÞing:
    1.  For a bill or motion to pass, a majority of all non-abstaining seats at the end of the voting period must have voted in favor, or half of all non-abstaining seats if the President approves the bill or motion within 24 hours of the end of the voting period.
    2.  Members of the CetÞing have the following options when voting:
        1.  In favor
        2.  Not in favor
        3.  Abstain
    3.  A bill may amend the Constitution if clearly marked as such by the presenter and if it receives two- thirds or more non-abstaining votes in favor.
4.  The following regulations apply to referenda:
    1.  A referendum becomes law after reaching a simple majority of non-abstaining votes in favor and an amount of non-abstaining voters greater than or equal to the square root of the number of citizens, floored, once it is concluded.
    2.  A referendum may amend the constitution if clearly marked as such by the presenter and if it receives two-thirds or more non-abstaining votes in favor and an amount of non-abstaining voters greater than or equal to the square root of the number of citizens, floored, once it is concluded.
    3.  Ballots have the following options:
        1.  In favor
        2.  Not in favor
        3.  Abstain
    4.  The abstain option does lower quorum.
5.  Basis of the CetÞing
    1.  The CetÞing has a number of seats that is equal to the population of Ceticilia divided by four and floored, but never exceeding thirty-six seats.
    2.  The CetÞing votes on approving appointments made by the President.
    3.  The CetÞing elects among its members a Speaker and Co-speaker to lead processes and enforce rules of conduct in the CetÞing. The decisions of the Speaker take precedence over those of the Co-speaker; otherwise they are equal.
6.  Basis of referenda
    1.  The population of Ceticilia may petition a new piece of legislation for referendum. The number of signatures needed is equal to the square root of the number of citizens, floored.
    2.  Members of the CetÞing may initiate referenda without petition.
    3.  The population of Ceticilia may petition a vote of no confidence to any political office holder. The number of signatures needed is equal to the square root of the number of citizens, floored.
7.  Vacancies in the CetÞing
    1.  Members of the CetÞing hold their positions unless they pass away, resign, lose their position in a parliamentary election, or their party decides to replace them.
    2.  Elections to the CetÞing must be held following a passed referendum calling for them to be held.
    3.  The referenda calling for an election to the CetÞing are to be initiated and conducted in the same fashion as those referenda which seek to create or amend laws.
    4.  Elections to the CetÞing will be held on the second full election period following the passing of the referendum.

# Article 7 -- Executive and Ministries

1.  The President is the Head of State and Government.
2.  The President may issue decrees to the executive regulating how ministries and bureaus act. Those decrees may not counter or override statute law, are subject to judicial review, and may be overturned by a simple majority in the legislature.
3.  The President may devolve their powers to ministries and offices. All ministerial and official decisions except for naturalization can be reverted by the President. The following regulations apply:
    1.  The creation of a ministry or office must be approved by the CetÞing with a simple majority vote.
    2.  Appointments by the Ceticilian President to a ministry or office must be approved by the CetÞing with a simple majority vote.
    3.  The executive powers of a ministry or office must be clearly outlined by the President.
    4.  Ministerial decrees may not override statute law, are subject to judicial review, and may be overturned by a simple majority in the legislature.
    5.  The President does not require approval from the CetÞing to create or make appointments to a ministry or office if the CetÞing is vacant or has not been duly elected.
4.  The President may veto all proposals made by the CetÞing and all referenda as long as they are not effective. These vetoes may be overturned by a majority of two-thirds in the CetÞing.
5.  The President is elected on the last full election period before 29 March.
6.  On alteration of this document, the executive is tasked with publishing an updated version of this document within one week of the alteration taking effect.
7.  The President may issue mandates which act as legislative statutes. These mandates may not counter or override existing statue law promulgated by the CetÞing or by referenda. Mandates are subject to judicial review and may be overturned by a simple majority in the CetÞing or by referendum.

# Article 8 -- Judiciary

1.  The judicial powers of Ceticilia are delegated to a Supreme Court and any lower courts established by the legislature.
2.  The Supreme Court consists of a Supreme Justice who is appointed by the President and confirmed by the CetÞing.
3.  The President may replace an incumbent Supreme Justice with absolute majority approval from the CetÞing.
4.  The Supreme Court's jurisdiction lies in matters of federal and Constitutional law.
5.  The Supreme Court may take or review cases of their own accord, but is required to fairly rule on any matter brought forth to them by the executive or legislature.
6.  In legal disputes between two or more parties, a fair trial must be held and recognized by the court, with competent representation guaranteed by the court to each side.

# Article 9 -- Indictment

1.  Indictment is defined as the loss of a political position after a vote of no confidence or after being found guilty on criminal offense.
2.  If any Member of the CetÞing does not comply with this law or other Ceticilian law that disqualifies them, the executive must immediately institute indictment proceedings against them, with the Supreme Court judging over them.
3.  If the President or any executive officer does not comply with this constitutional law or other Ceticilian law that disqualifies them, the CetÞing must immediately institute indictment proceedings against them, with the Supreme Court judging over them.
4.  If any Judge willingly and actively does not comply with this constitutional law or Ceticilian law that disqualifies them, the executive and the CetÞing must immediately institute indictment proceedings against them, with the CetÞing judging over them.

# Article 10 -- Environment

1.  Ceticilia acts in conscience of the environment and of the foundations of life of contemporary and future generations.
2.  Ceticilian urban development must not destroy local wildlife or disturb channels that are vital to a species\' survival.
3.  Industrial production of dairy and meat is outlawed.
4.  Industrial production of energy is only allowed if it does not use combustion as its main source.

# Article 11 - Separation of Powers

1.  Judges may only pertain to the judicial branch of government.
2.  Executive officers may only pertain to the executive and legislative branches of government. The President may only pertain to the executive branch and those positions laid out by this constitution.
3.  A breach of separation of powers is not observed in the case of an executive or judicial officer proposing or effecting a law or change to the law via mechanisms of referendum or petition.