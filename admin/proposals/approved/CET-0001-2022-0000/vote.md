---
title: Vote for CET 0001-2022-0000
tglink-start: https://t.me/c/1051110288/118332?thread=118317
tglink-conclusion: https://t.me/c/1051110288/118375?thread=118317
date-started: 2022-12-11 20:32:08
date-concluded: 2022-12-12 21:27:12
---

| Option       | Count |
|--------------|-------|
| In Favor     | 4     |
| Not in favor | 1     |
| Abstain      | 1     |
