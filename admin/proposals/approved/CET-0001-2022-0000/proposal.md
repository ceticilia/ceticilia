---
title: CET 0001-2022-0000 -- Legislative formatting
tglink: https://t.me/c/1051110288/118317
date: 2022-12-11 19:58:00
author: Astrex
---

All legislation must be written and interpreted as a Boolean
expression followed by a conditional expression which specifies the
consequences of the law evaluating to true or false. Legislation may
have official natural language translations, but they may not
contradict the original Boolean expression.

```
{the legislation is written and interpreted as a Boolean expression} AND
{the Boolean expression is followed by a conditional expression which specifies the consequences of the law evaluating to true or false} AND
(
    {the legislation has one or more official natural language translations} NAND
    {any official natural language translation of the legislation contradicts the original Boolean expression}
) ?
the legislation may be passed :
the legislation may not be passed
```
