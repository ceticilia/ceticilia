---
title: Vote for CET 0001-2022-0003
tglink-start: https://t.me/c/1051110288/118981?thread=118980
tglink-conclusion: https://t.me/c/1051110288/118983?thread=118980
date-started: 2022-12-18 18:09:54
date-concluded: 2022-12-19 18:11:00
---

| Option       | Count |
|--------------|-------|
| In Favor     | 2     |
| Not in favor | 0     |
| Abstain      | 1     |
