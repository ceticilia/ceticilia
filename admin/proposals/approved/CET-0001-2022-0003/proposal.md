---
title: CET 0001-2022-0003
tglink: https://t.me/c/1051110288/118980
date: 2022-12-18 18:09:10
author: Astrex
---

CET 0001-2022-0003

- Key to reading amendments:
- <u>Additions are underlined</u>
- <s>Deletions are struck through</s>
- [Square brackets represent the CET ID of the amendment that caused the addition/deletion]

# Amendment to CET 0001-2022-0000

<s>
```
for each item of legislation:[CET 0001-2022-0002]
{the legislation is written and interpreted as a Boolean expression} AND
{the Boolean expression is followed by a conditional expression which specifies the consequences of the law evaluating to true or false} AND
(
    {the legislation has one or more official natural language translations} NAND
    {any official natural language translation of the legislation contradicts the original Boolean expression}
) ?
the legislation may be passed :
the legislation may not be passed[CET 0001-2022-0003]
```
<s>

# Amendment to CET 0001-2022-0001

<s>
```
for each item of legislation:
{the legislation declares the scope of its effectiveness} ?
the legislation may be passed :
the legislation may not be passed[CET 0001-2022-0003]
```
</s>
