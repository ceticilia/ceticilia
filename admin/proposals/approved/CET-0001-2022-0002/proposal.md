---
title: CET 0001-2022-0002 -- Amendment to CET 0001-2022-0000
tglink: https://t.me/c/1051110288/118404
date: 2022-12-13 15:36:10
author: Astrex
---


Additions are underlined

Deletions are struck through

[Square brackets represent the CET ID of the amendment that caused the addition/deletion]

————————————————

for each item of legislation:[CET 0001-2022-0002]
{the legislation is written and interpreted as a Boolean expression} AND
{the Boolean expression is followed by a conditional expression which specifies the consequences of the law evaluating to true or false} AND
(
    {the legislation has one or more official natural language translations} NAND
    {any official natural language translation of the legislation contradicts the original Boolean expression}
) ?
the legislation may be passed :
the legislation may not be passed
