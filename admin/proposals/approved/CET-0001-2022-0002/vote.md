---
title: Vote for CET 0001-2022-0002
tglink-start:
tglink-conclusion:
date-started: 2022-12-13 15:37:15
date-concluded: 2022-12-14 15:56:13
---

| Option       | Count |
|--------------|-------|
| In favor     | 1     |
| Not in favor | 0     |
| Abstain      | 0     |
