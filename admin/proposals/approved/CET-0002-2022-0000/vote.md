---
title: Vote for CET 0002-2022-0000
tglink-start: https://t.me/c/1051110288/118989?thread=118988
tglink-conclusion: https://t.me/c/1051110288/119010?thread=118988
date-started: 2022-12-19 22:30:00
date-concluded: 2022-12-20 23:48:08
---

| Option       | Count |
|--------------|-------|
| In Favor     | 5     |
| Not in favor | 0     |
| Abstain      | 0     |
