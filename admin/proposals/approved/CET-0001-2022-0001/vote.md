---
title: Vote for CET 0001-2022-0001
tglink-start: https://t.me/c/1051110288/118399?thread=118398
tglink-conclusion: https://t.me/c/1051110288/118423?thread=118398
date-started: 2022-12-13 15:25:27
date-concluded: 2022-12-14 15:52:44
---

| Option       | Count |
|--------------|-------|
| In Favor     | 2     |
| Not in favor | 0     |
| Abstain      | 1     |
