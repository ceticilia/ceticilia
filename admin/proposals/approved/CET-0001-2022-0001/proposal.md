---
title: CET 0001-2022-0001
tglink: https://t.me/c/1051110288/118398
date: 2022-12-13 15:24:46
author: Astrex
---

# ENGLISH

Each item of legislation must declare the scope of its effectiveness.

# BOOLEAN

```
for each item of legislation:
{the legislation declares the scope of its effectiveness} ?
the legislation may be passed :
the legislation may not be passed
```
