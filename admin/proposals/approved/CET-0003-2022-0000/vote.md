---
title: Vote for CET 0003-2022-0000
tglink-start: https://t.me/c/1051110288/119738?thread=119737
tglink-conclusion: https://t.me/c/1051110288/119800?thread=119737
date-started: 2022-12-26 04:45:38
date-concluded: 2022-12-27 06:33:45
---

| Option       | Count |
|--------------|-------|
| In Favor     | 5     |
| Not in favor | 0     |
| Abstain      | 1     |
